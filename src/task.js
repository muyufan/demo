import db from 'sqlite';
import Log from './log';
import { ipcMain } from 'electron';

const STATUS = {
  NEW: 0,
  FAILED: -1,
  CANCELED: -2,
  CANSAVED: 20,
  BOXSAVED: 40,
  VGSAVED: 60,
  DIFFED12: 70,
  DIFFED23: 80,
  ANALYZED12: 90,
  ANALYZED23: 100,
};

export default class Task {
  constructor(options = {}) {
    this.options = options;
    this.canlog = new Log('can', this.options.canlogDir);
    this.boxlog = new Log('box', this.options.boxlogDir);
    this.vglog = new Log('vg', this.options.vglogDir);

    this.status = STATUS.NEW;
  }

  initDB() {
    this.db = db.open(`result${this.options.id}`);
  }

  initTable() {
    this.db.run('CREATE TABLE can (timestamp INTEGER, data TEXT)');
    this.db.run('CREATE TABLE box (timestamp INTEGER, data TEXT)');
    this.db.run('CREATE TABLE vg (timestamp INTEGER, data TEXT)');
  }

  getStatus() {
    return this.status;
  }

  static addTasks(task) {
    let p = db.open(`${__dirname}/db/db`);
    p = p.then(() => db.exec(`INSERT INTO "main"."task" ("vin","from","to","can_log_dir","box_log_dir") VALUES ("${task.vin}","${task.from}","${task.to}","${task.canlogdir}","${task.boxlogdir}");`));
    p = p.then(() => db.close());
  }

  static deleteTasks(task) {
    let p = db.open(`${__dirname}/db/db`);
    p = p.then(() => db.exec(`DELETE FROM "main"."task" WHERE "id" = "${task.id}";`));
    p = p.then(() => db.close());
  }
  static getTasks(event) {
    let p = db.open(`${__dirname}/db/db`);
    p = p.then(() => db.all('SELECT * FROM "main"."task";')).then((result) => {
      console.log('tasks select from sqlite: ', result);
      event.returnValue = result;
    });
    p = p.then(() => db.close());
  }

  run() {
    this.initDB();
    this.initTable();
    this.canlog.save();
    this.status = STATUS.CANSAVED;
    ipcMain.on('task-status-request', (event, arg) => {
      console.log(arg);
      event.sender.send('task-status-response', this.status);
    });
    this.boxlog.save();
    this.status = STATUS.BOXSAVED;
    this.vglog.save();
    this.status = STATUS.VGSAVED;
    this.diff();
    this.analyze();
    this.status = STATUS.DONE;
  }

  diff() {
    this.db.run('CREATE TABLE RESULT12 AS SELECT * FROM can LEFT JOIN box ON can.timestamp == box.timestamp;');
    this.db.get('SELECT count(timestamp) FROM resultTable WHERE data:1 ISNULL;', (row) => {
      console.log(row);
    });
    this.status = STATUS.DIFFED12;
    this.db.run('CREATE TABLE RESULT23 AS SELECT * FROM box LEFT JOIN vg ON box.timestamp == vg.timestamp;');
    this.status = STATUS.DIFFED23;
  }

  analyze() {
    this.status = STATUS.ANALYZED12;
    this.status = STATUS.ANALYZED23;
  }

  cancel() {
    this.status = STATUS.CANCELED;
  }

  delete() {
    this.status = STATUS.CANCELED;
  }
}

