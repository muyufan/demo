import fs from 'fs';

class Log {
  constructor(type, dir) {
    this.type = type;
    this.dir = dir;
  }

  save() {
    fs.readdir(this.dir, this.saveCANLog);
  }

  saveCANLog(error, files) {
    console.log(files);
  }

  saveBoxLog() {}
  saveVGLog() {}
}

export default Log;
