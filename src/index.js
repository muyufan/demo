import electron, { app, BrowserWindow, ipcMain } from 'electron
import Task from './task';
import Vehicle from './vehicle';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

const createWindow = () => {
  // Create the browser window.
  const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize;
  mainWindow = new BrowserWindow({ width, height });

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/../web/build/index.html`);

  // Open the DevTools.
  mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
ipcMain.on('get-request', (event, arg) => {
  switch (arg) {
    case 'vehicles':
      Vehicle.getVehicles(event);
      break;
    case 'tasks':
      Task.getTasks(event);
      break;
    default:
      console.log(arg);
      break;
  }
});

ipcMain.on('delete-request', (event, arg) => {
  let type = '';
  if (arg.startsWith('task')) {
    type = 'task';
  }
  if (arg.startsWith('vehicle')) {
    type = 'vehicle';
  }
  switch (type) {
    case 'vehicles':
      Vehicle.deleteVehicle(event);
      break;
    case 'tasks':
      Task.deleteTask(event);
      break;
    default:
      console.log(arg);
      break;
  }
});

ipcMain.on('add-vehicle-request', (event, vehicle) => {
  console.log(vehicle);
  Vehicle.addVehicle(vehicle);
  event.sender.send('add-vehicle-reply', 'success');
});

ipcMain.on('add-task-request', (event, data) => {
  console.log('post data from web addTaskForm: ', data)
  Task.addTasks({
    vin: data.vin,
    from: data.range[0]._d,
    to: data.range[1]._d,    
    canlogdir: data.canlogdir,
    boxlogdir: data.boxlogdir,
  });
  event.sender.send('add-task-reply', 'success');
});

ipcMain.on('runtask-request', (event, data) => {
  console.log('post data from web addTaskForm: ', data)
  const task = Task.addTasks({
    vin: data.vin,
    from: data.range[0]._d,
    to: data.range[1]._d,    
    canlogdir: data.canlogdir,
    boxlogdir: data.boxlogdir,
  });

  event.sender.send('runtask-status', task.getStatus());
});