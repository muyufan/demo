import db from 'sqlite';

export default class Vehicle {
  constructor(vehicle = {}) {
    this.vehicle = vehicle;
  }

  static addVehicle(vehicle) {
    let p = db.open(`${__dirname}/db/db`);
    p = p.then(() => db.exec(`INSERT INTO "main"."vehicle" ("vin") VALUES (${vehicle.vin});`));
    p = p.then(() => db.close());
  }

  static getVehicles(event) {
    let p = db.open(`${__dirname}/db/db`);
    p = p.then(() => db.all('SELECT * FROM "main"."vehicle";')).then((result) => {
      console.log('vehicles select from sqlite: ', result);
      event.returnValue = result;
    });
    p = p.then(() => db.close());
  }
  
}
