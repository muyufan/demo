import React from "react";
import { Route, Switch } from "react-router-dom";

import Tasks from "./containers/Tasks";
import Vehicles from "./containers/Vehicles";


export default () =>
  <Switch>
    <Route path="/" exact component={Tasks} />
    <Route path="/vehicles" exact component={Vehicles} />

  </Switch>;