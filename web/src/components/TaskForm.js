import React from 'react';
import { Icon, Upload, DatePicker, Modal, Form, Button, Checkbox, Input, Radio } from 'antd';
import moment from 'moment';

const FormItem = Form.Item;

const TaskForm = Form.create()(
  (props) => {
    const { visible, onCancel, onCreate, form } = props;
    const { getFieldDecorator } = form;
    return (
      <Modal
        visible={visible}
        title="创建一个新任务"
        okText="确认"
        onCancel={onCancel}
        onOk={onCreate}
      >
        <Form>
          <FormItem label="Vin">
            {getFieldDecorator('vin', {
              rules: [{ 
                required: true, message: '请填写必填项！',
                len: 17, message: 'Vin长度为17位。'
              }],
            })(
              <Input />
            )}
          </FormItem>
          <FormItem label="日期范围">
            {getFieldDecorator('range', {
              rules: [{ required: true, message: '请填写必填项！' }],
            })(
              <DatePicker.RangePicker
                ranges={{ 今天: [moment(), moment()], '本周': [moment(), moment().endOf('week')], '当月': [moment(), moment().endOf('month')] }}
                showTime
                format="YYYY/MM/DD HH:mm:ss"
              />
            )}
          </FormItem>
          <FormItem label="CAN日志目录">
            {getFieldDecorator('canlogdir', {
              
            })(
              <Input />
            )}
          </FormItem>
          <FormItem label="BOX日志目录">
            {getFieldDecorator('boxlogdir', {

            })(
              <Input />
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
);

export default TaskForm;
