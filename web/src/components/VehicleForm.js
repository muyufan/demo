import React from 'react';
import { Icon, Upload, DatePicker, Modal, Form, Button, Checkbox, Input, Radio } from 'antd';
import moment from 'moment';

//import {ipcRenderer} from 'electron'

const FormItem = Form.Item;


//const runTask = function name(params) {
//  ipcRenderer.send('asynchronous-message', 'ping')
//}

const VehicleForm = Form.create()(
  (props) => {
    const { visible, onCancel, onCreate, form } = props;
    const { getFieldDecorator } = form;
    return (
      <Modal
        visible={visible}
        title="创建一个新车辆"
        okText="确认"
        onCancel={onCancel}
        onOk={onCreate}
      >
        <Form>
          <FormItem label="Vin">
            {getFieldDecorator('vin', {
              rules: [{ 
                required: true, message: '请填写必填项！',
                len: 17, message: 'Vin长度为17位。'
              }],
            })(
              <Input />
            )}
          </FormItem>



        </Form>
      </Modal>
    );
  }
);

export default VehicleForm;
