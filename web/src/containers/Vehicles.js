import React from 'react';
import { Modal, Progress } from 'antd';
import { Layout, Table, Button, Form, Icon } from 'antd';

import VehicleForm from '../components/VehicleForm';
const { ipcRenderer } = window.require('electron');

const columns = [{
  title: '车辆识别码',
  dataIndex: 'vin',
  render: text => <a href="#">{text}</a>,
}, {
  title: '部门',
  dataIndex: 'department',
}, {
  title: '环境',
  dataIndex: 'env',
}, {
  title: '车型',
  dataIndex: 'model',
}, {
  title: 'GW',
  dataIndex: 'gw',
}, {
  title: 'BMS',
  dataIndex: 'bms',
}, {
  title: 'LE',
  dataIndex: 'le',
}, {
  title: 'LM',
  dataIndex: 'lm',
}, {
  title: '电池编码',
  dataIndex: 'batcode',
}, {
  title: 'SN',
  dataIndex: 'sn',
}, {
  title: 'IMEI',
  dataIndex: 'imei',
}, {
  title: 'BOX-SW',
  dataIndex: 'boxws',
}, {
  title: 'BOX-HW',
  dataIndex: 'boxhw',
}, {
  title: 'ICCID',
  dataIndex: 'iccid',
}, {
  title: '操作',
  key: 'action',
  render: (text, record) => (
    <span>
      <a href="#"><Icon type="edit" /></a>
      <span className="ant-divider" />
      <a href="#"><Icon type="close-circle" /></a>
    </span>
  ),
}];

// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    disabled: record.name === 'Disabled User',    // Column configuration not to be checked
  }),
};

class Vehicles extends React.Component {
  state = {
    formVisible: false,
    data: [],
  };
  showVehicleForm = () => {
    this.setState({ formVisible: true });
  }
  handleCancel = () => {
    this.setState({ formVisible: false });
  }
  handleCreate = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      console.log('Received values of form: ', values);      
      if (err) {
        return;
      }

      ipcRenderer.send('add-vehicle-request', values);
      ipcRenderer.on('add-vehicle-reply', (event, result) => {
        console.log('Add vehicle result: ', result)
        if (result === 'success') {
          form.resetFields();
          this.setState({ formVisible: false });
          this.fetch();
        }
      });
    });
  }
  saveFormRef = (form) => {
    this.form = form;
  }
  showDelete() {
    Modal.confirm({
      title: '你确定要删除这个车辆吗?',
      content: '删除后将无法恢复',
      okText: '确认',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  fetch() {
    const result = ipcRenderer.sendSync('get-request', 'vehicles');
    console.log('Get vehicles result: ', result);
    this.setState({ data: result });
  }

  componentDidMount() {
    this.fetch();
  }
  render() {
    return (
      <Layout style={{ padding: '0 10px' }}>
        <div>
          <Button type="primary" onClick={this.showDelete} style={{ float: 'right', margin: '10px 10px'}}>删除车辆</Button>
          <Button type="primary" onClick={this.showVehicleForm} style={{ float: 'right', margin: '10px 10px'}}>添加车辆</Button>
          <VehicleForm
            ref={this.saveFormRef}
            visible={this.state.formVisible}
            onCancel={this.handleCancel}
            onCreate={this.handleCreate}
          />
        </div>
        <div style={{ background: '#fff', minHeight: 360 }}>
          <Table rowSelection={rowSelection} columns={columns} dataSource={this.state.data} pagination={{ pageSize: 7 }} bordered={true} />
        </div>
      </Layout>
    );
  }
}

export default Vehicles;
