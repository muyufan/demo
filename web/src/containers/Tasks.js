import React from 'react';
import { Modal, Progress } from 'antd';
import { Layout, Table, Button, Form, Icon } from 'antd';

import TaskForm from '../components/TaskForm';
const { ipcRenderer } = window.require('electron');


const columns = [{
  title: '车辆识别码',
  dataIndex: 'vin',
  render: text => <a href="#">{text}</a>,
}, {
  title: '部门',
  dataIndex: 'department',
}, {
  title: '环境',
  dataIndex: 'env',
}, {
  title: '开始时间',
  dataIndex: 'from',
}, {
  title: '结束时间',
  dataIndex: 'to',
}, {
  title: '输出文件夹',
  dataIndex: 'outputdir',
}, {
  title: '状态',
  dataIndex: 'status',
}, {
  title: '进度',
  key: 'progress',
  render: (text, record) => (
    <div style={{ 'white-space': 'nowrap' }}>
      <Progress percent={50} status={record.status} />
    </div>
  )
}, {
  title: '操作',
  key: 'action',
  render: (text, record) => (
    <span>
      <a alt="执行任务" href="#"><Icon type="play-circle" /></a>
      <span className="ant-divider" />
      <a alt="取消任务"  href="#"><Icon type="minus-circle" /></a>
      <span className="ant-divider" />
      <a alt="删除任务"  href="#"><Icon type="close-circle" /></a>
      <span className="ant-divider" />
    </span>
  ),
}];

// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    disabled: record.name === 'Disabled User',    // Column configuration not to be checked
  }),
};

class Tasks extends React.Component {
  state = {
    formVisible: false,
    data:   [],
  };
  showTaskForm = () => {
    this.setState({ formVisible: true });
  }
  handleCancel = () => {
    this.setState({ formVisible: false });
  }
  handleCreate = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      console.log('Received values of form: ', values);      
      if (err) {
        return;
      }

      ipcRenderer.send('add-task-request', values);
      ipcRenderer.on('add-task-reply', (event, result) => {
        console.log('Add task result: ', result)
        if (result === 'success') {
          form.resetFields();
          this.setState({ formVisible: false });
          this.fetch();
        }
      });
    });
  }

  handleDelete = (taskId) => {
    ipcRenderer.send('delete-request', `task${taskId}`);
    ipcRenderer.on('delete-reply', (event, result) => {
      console.log('Deleting task result: ', result)
      if (result === 'success') {
        this.fetch();
      }
    });
  }

  runTask = (taskId) => {
    ipcRenderer.send('runtask-request', `task${taskId}`);
    ipcRenderer.on('runtask-status', (event, status) => {
      console.log('Running task status: ', status)
      if (status === 'success') {
        this.fetch();
      }
    });
  }

  saveFormRef = (form) => {
    this.form = form;
  }
  showDelete() {
    Modal.confirm({
      title: '你确定要删除这个任务吗?',
      content: '删除后将无法恢复',
      okText: '确认',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        console.log('Deleting task');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  showCancel() {
    Modal.confirm({
      title: '你确定要取消这个任务吗?',
      content: '取消后将无法恢复',
      okText: '确认',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  fetch() {
    const result = ipcRenderer.sendSync('get-request', 'tasks');
    console.log('Get tasks result: ', result);
    this.setState({ data: result });
  }

  componentDidMount() {
    this.fetch();
  }

  render() {
    return (
      <Layout style={{ padding: '0 10px' }}>
        <div>
          <Button type="primary" onClick={this.runTask} style={{ float: 'right', margin: '10px 10px'}}>开始任务</Button>
          <Button type="primary" onClick={this.showCancel} style={{ float: 'right', margin: '10px 10px'}}>取消任务</Button>
          <Button type="primary" onClick={this.showDelete} style={{ float: 'right', margin: '10px 10px'}}>删除任务</Button>
          <Button type="primary" onClick={this.showTaskForm} style={{ float: 'right', margin: '10px 10px'}}>添加任务</Button>
          <TaskForm
            ref={this.saveFormRef}
            visible={this.state.formVisible}
            onCancel={this.handleCancel}
            onCreate={this.handleCreate}
          />
        </div>
        <div style={{ background: '#fff', minHeight: 360 }}>
          <Table rowSelection={rowSelection} columns={columns} dataSource={this.state.data} pagination={{ pageSize: 7 }} bordered={true} />
        </div>
      </Layout>
    );
  }
}

export default Tasks;
