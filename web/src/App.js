import React from 'react';
import { Link, withRouter } from "react-router-dom";

import { Layout, Menu, Icon } from 'antd';

import Routes from "./Routes";
import './App.css';

const SubMenu = Menu.SubMenu;
const { Sider } = Layout;

class App extends React.Component {
  state = {
    collapsed: true,
  };

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  }
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" >
            <Menu.Item key="1">
              <Icon type="home" />
              <Link to="/"><span>主页</span></Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="database" />
              <span>任务</span>
            </Menu.Item>
            <Menu.Item key="3">
              <Icon type="car" />
              <Link to="/vehicles"><span>车辆</span></Link>
            </Menu.Item>
            <SubMenu
              key="sub1"
              title={<span><Icon type="search" /><span>分析</span></span>}
            >
              <Menu.Item key="3">自动分析</Menu.Item>
              <Menu.Item key="4">手动分析</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout>
          <Routes />
        </Layout>
      </Layout>
    );
  }
}

export default withRouter(App);
